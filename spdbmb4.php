<?php
/*
Plugin Name: Simple:Press DB MB4 Encoder
Plugin URI: http://simple-press.com
Description: Convert Simple:Press database tables to use utf8mb4 character encoding
Version: 1.0
Author: Andy Staines & Steve Klasen
Author URI: http://simple-press.com
WordPress Version 4.4.2 and above
Simple-Press Version 5.5.8 and above
*/

/*  Copyright 2015  Andy Staines & Steve Klasen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For a copy of the GNU General Public License, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

# ------------------------------------------------------------------
# Define Constants
# ------------------------------------------------------------------
	define('SPDBMB4URL',			WP_PLUGIN_URL.'/sp-db-mb4/');	# Plugin folder url
	define('SPDBMB4DIR',			WP_PLUGIN_DIR.'/sp-db-mb4/');	# Plugin path

# ------------------------------------------------------------------
# Action/Filter Hooks
# Create new menu item in the WP Simple:Press menu
# ------------------------------------------------------------------
add_action('admin_menu', 				'spdbmb4_menu');

# ------------------------------------------------------------------------------------------
# Determine quickly if admin and then if importer admin page load

if(is_admin() && isset($_GET['page']) && stristr($_GET['page'], 'sp-db-mb4/')) {
	add_action('admin_print_styles', 	'spdbmb4_css');
	add_action('admin_enqueue_scripts', 'spdbmb4_scripts');
}

# ------------------------------------------------------------------
# Add the importer to SP menu item into th SP menu
# ------------------------------------------------------------------
function spdbmb4_menu() {
	$url = 'sp-db-mb4/admin/spdbmb4-setup.php';
	add_submenu_page('simple-press/admin/panel-forums/spa-forums.php', esc_attr('DB MB4'), esc_attr('DB MB4 Encoder'), 'read', $url);
}

# ------------------------------------------------------------------
# Loads up the forum admin CSS
# ------------------------------------------------------------------
function spdbmb4_css() {
	wp_register_style('spDBMB4Style', SPDBMB4URL.'css/spdbmb4.css');
	wp_enqueue_style( 'spDBMB4Style');
}

# ------------------------------------------------------------------
# Load up the javascript we need
# ------------------------------------------------------------------
function spdbmb4_scripts() {
	wp_register_script('jquery', WPINC.'/js/jquery/jquery.js', false, false, false);
	wp_enqueue_script('jquery');
	wp_enqueue_script('spdbmb4', SPDBMB4URL.'jscript/spdbmb4.js', array('jquery'), false);
}

function spdbmb4_compare($need, $got) {
	$need= explode('.', $need);
	$got = explode('.', $got);

	if (isset($need[0]) && intval($need[0]) > intval($got[0])) return false;
	if (isset($need[0]) && intval($need[0]) < intval($got[0])) return true;

	if (isset($need[1]) && intval($need[1]) > intval($got[1])) return false;
	if (isset($need[1]) && intval($need[1]) < intval($got[1])) return true;

	if (isset($need[2]) && intval($need[2]) > intval($got[2])) return false;
	return true;
}

?>