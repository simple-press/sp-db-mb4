<?php

?>
	<div class="wrap nosubsub">
	<div id="icon-plugins" class="icon32"><br /></div>
	<h2>Simple:Press MB4 Table Encoder</h2>
	<div style="clear: both"></div>
	<div id="spdbContainer">
	<div id="spdbMainHead">
	<h1>Convert Simple:Press Tables/Columns to Utf8mb4 Encoding</h1>
	<div style="clear: both"></div>
	</div><br />
<?php
	sp_dbmb4_form();
?>
	</div></div>
<?php

function sp_dbmb4_form() {
	global $wpdb;

	# quick version check
	$mysql_required = (string) '5.5.3';
	if (spdbmb4_compare($mysql_required, $wpdb->db_version()) == false) {
?>
		<div id="spdbForm" class="spdbMainPanel">
		<h3><?php printf('%s Version %s Installed', 'MySQL', $wpdb->db_version()); ?></h3>
		<p>Your version of MySQL does not support utf8mb4 encoding<br />
		<?php printf('%s version %s or above is required', 'MySQL', $mysql_required); ?></p><br />
		</div>
<?php
		die();
	}

	# Version OK

	$sql = "SHOW TABLE STATUS WHERE Name LIKE '".$wpdb->prefix."sf%'";
	$tables = $wpdb->get_results($sql);

	if($tables) {
		$sql = "SHOW TABLES LIKE '".$wpdb->prefix."sf%'";
		$tables = $wpdb->get_col($sql);
?>
		<div id="spdbForm" class="spdbMainPanel">
		<p>This process will convert your Simple:Press database tables <br />
		and columns to use the urt8mb4 (4 byte) encoding.<br /></p>
		<h4>IMPORTANT</h4><ul>
		<li>It is strongly recommended that you make a full <u>database backup</u> prior to <br />
		running this conversion. Remember - you may need to restore it!</li>
		<li>For safety and to ensure no data corruption you should place your site<br />
		in <u>Maintenance Mode</u>. There is a simple and effective <a href="http://sw-guide.de/wordpress/plugins/maintenance-mode/">WordPress plugin</a> to enable this.</li></ul>
<?php
		$phpfile = SPDBMB4URL."admin/spdbmb4-process.php";
		$tables = implode(',', $tables);
		$target = '#spdbForm';

	} else {
?>
		<div id="spdbForm" class="spdbMainPanel">
		<p>There are no Simple:Press tables found requiring encoding</op>
		</div>
<?php
		return;
	}
?>
	<input type="button" class="button-primary" id="doit" name="doit" value="Convert Now" onclick="spjPerformMB4('<?php echo($phpfile);?>', '<?php echo($tables);?>', '<?php echo($target);?>');";/>
	</div>
<?php
}

?>