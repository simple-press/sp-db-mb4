<?php

require_once("../../../../wp-config.php");
global $wpdb;

$t = esc_sql($_GET['t']);

$sql = "SHOW TABLE STATUS WHERE NAME = '$t'";
$check = $wpdb->get_row($sql);

$table = ($check) ? $check->Name : '';

if($table) {
	$results = $wpdb->get_results( "SHOW FULL COLUMNS FROM `$table`" );
	if ( ! $results ) {
		return false;
	}

	foreach ( $results as $column ) {
		if ( $column->Collation ) {
			list( $charset ) = explode( '_', $column->Collation );
			$charset = strtolower( $charset );
			if ( 'utf8' !== $charset && 'utf8mb4' !== $charset ) {
				# Don't upgrade tables that have non-utf8 columns.
				echo '<tr><td>'.$t.'</td><td class="spdbSuccess">No Conversion Required</td></tr>';
				die();
			}
		}
	}

	$table_details = $wpdb->get_row( "SHOW TABLE STATUS LIKE '$table'" );
	if ( ! $table_details ) {
		die();
	}

	list( $table_charset ) = explode( '_', $table_details->Collation );
	$table_charset = strtolower( $table_charset );
	if ( 'utf8mb4' === $table_charset ) {
		echo '<tr><td>'.$t.'</td><td class="spdbSuccess">Already has utf8mb4 Encoding</td></tr>';
		die();
	}

	$wpdb->query( "ALTER TABLE $table CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci" );
	echo '<tr><td>'.$t.'</td><td class="spdbSuccess">Converted to utf8mb4</td></tr>';

} else {

	echo '<tr><td>'.$t.'</td><td class"spdbFailure">Not found in the database';

}

die();

?>